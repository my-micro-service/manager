package cn.smile.manager.mq.controller;

import cn.smile.commons.bean.from.mq.MqMessageFrom;
import cn.smile.commons.response.ResponseResult;
import cn.smile.manager.mq.config.MqConfig;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *
 * </p>
 *
 * @author longjuntao
 * @since 2020/12/1 16:13
 */
@RestController
@RequestMapping(value = "testRabbitMq")
public class TestRabbitMqController {

    private static final Logger logger = LoggerFactory.getLogger(TestRabbitMqController.class);

    @Resource
    private MqConfig mqConfig;

    @PostMapping("sendMq")
    public ResponseResult sendMq(@RequestBody MqMessageFrom message) {
        logger.info("[TestRabbitMqController].[sendMq]------> Send RabbitMQ Message Start, message = {}", JSON.toJSONString(message));
        mqConfig.sendMessage(message.getQueueName(), message.getContext());
        return ResponseResult.success();
    }


}
