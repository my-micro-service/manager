package cn.smile.manager.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * <p>
 * RabbitMq启动类(排除数据库配置)
 * </p>
 *
 * @author longjuntao
 * @since 2020/12/1 15:09
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class RabbitMqManagerApplication {

    private static final Logger logger = LoggerFactory.getLogger(RabbitMqManagerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(RabbitMqManagerApplication.class, args);
        logger.info("---------------------RabbitMqManagerApplication Successful Start---------------------");
    }
}
