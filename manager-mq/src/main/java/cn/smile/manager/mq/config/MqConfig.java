package cn.smile.manager.mq.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.retry.MessageRecoverer;
import org.springframework.amqp.rabbit.retry.RepublishMessageRecoverer;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

/**
 * <p>
 *
 * </p>
 *
 * @author longjuntao
 * @since 2020/12/1 16:26
 */
@Component
public class MqConfig {

    private static final Logger logger = LoggerFactory.getLogger(MqConfig.class);

    private static final String EX_CHANGE_MSG_ERROR = "exChangeMsgError";

    private static final String ROUTING_KEY_ERROR = "routingKeyError";

    @Resource
    private RabbitAdmin rabbitAdmin;

    @Bean
    public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
        return new RabbitAdmin(connectionFactory);
    }

    @Bean
    public MessageRecoverer messageRecoverer(RabbitTemplate rabbitTemplate) {
        return new RepublishMessageRecoverer(rabbitTemplate, EX_CHANGE_MSG_ERROR, ROUTING_KEY_ERROR);
    }

    /**
     * 发送消息到指定MQ队列
     * @param queueName 队列名称
     * @param message 消息
     */
    public void sendMessage(String queueName, String message) {

        logger.info("[MqUtils].[sendMessage] ------> queueName = {}, message = {}", queueName, message);

        this.declareBinding(queueName, queueName);
        rabbitAdmin.getRabbitTemplate().convertAndSend(queueName, queueName, message);
    }

    private void declareBinding(String exChangeName, String queueName) {
        if (StringUtils.isEmpty(rabbitAdmin.getQueueProperties(queueName))) {
            Queue queue = new Queue(queueName, true, false, false, null);
            rabbitAdmin.declareQueue(queue);
            TopicExchange directExChange = new TopicExchange(exChangeName);
            rabbitAdmin.declareExchange(directExChange);
            Binding binding = BindingBuilder.bind(queue).to(directExChange).with(queueName);
            rabbitAdmin.declareBinding(binding);
        }
    }
}
