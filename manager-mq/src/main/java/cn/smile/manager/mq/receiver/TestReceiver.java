package cn.smile.manager.mq.receiver;

import cn.smile.commons.constant.CommonConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 测试消息消费者
 * </p>
 *
 * @author longjuntao
 * @since 2020/12/1 16:10
 */
@Component
@RabbitListener(queues = CommonConstant.QUEUE_NAME)
public class TestReceiver {

    private static final Logger logger = LoggerFactory.getLogger(TestReceiver.class);

    @RabbitHandler
    public void process(String temp) {
        logger.info("[TestReceiver].[process] temp = {}", temp);
    }
}
